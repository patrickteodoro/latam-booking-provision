WORKSTATION PROVISION
---

1. Você precisará instalar o `ansible` primeiro.

2. Crie o arquivo `inventory/workstation` com o conteudo abaixo:

	[all]
	IP ansible_port=PORTA_SSH ansible_ssh_user=USER_SSH

Substitua o IP, PORTA_SSH e USER_SSH com as informacoes da workstation.
As chaves *ansible_port* e *ansible_ssh_user* são opcionais mas você tera que informar essas configuracoes pela linha de comando quando executar o `ansible-playbook` ou no arquivo de configuracao `ansible.cfg`.

3. Executa:

	ansible-playbook workstation